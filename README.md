Altocloud Examples

Event Tracking Code Snippets:

ScrollToBottom tag
This event tag is used to engage users who scroll to the bottom of a page.

TimeOnPage6Secs tag
This event tag is used to segment users who wait 6 seconds.

TimerHasFired tag
This event tag is used to segment users who wait too long.