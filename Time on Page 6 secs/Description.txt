Information taken from the following URL: https://all.docs.genesys.com/ATC/Current/Event/6_Secs

Scenario: Segment users who wait 6 seconds.

To build a segment of users who wait 6 seconds on a page:
1) Use JavaScript to create a create a timeOnPage6Secs event tag to record when a timer fires.
2) Deploy the event tag with your preferred tag manager.
3) Create a TimeonPage6Secs segment that uses the event tag.
4) Test your solution in LiveNow.

Create a timeOnPage6Secs event tag:
In your preferred code editor, develop and validate a timerOnPage6Secs event tag.
In the example:
* ac('record') is the function name.
* timerOnPage6Secs is your event tag.

Deploy the event tag:
Deploy your event tag using your preferred tag manager:
* Adobe Launch
* Google Tag Manager

Note: The JavaScript code that you define for an event tag will be executed every time a customer visits a page that matches the execution condition you defined for the tag (either the specific page URL or the presence of an HTML element.)

Create a TimeonPage6Secs segment:
1) In Segments, under Visitor journey, click Add first activity.
2) In the Attribute box, select Event name.
3) In the Operator box, select equals.
4) In the Value box, type the name for your tag as you defined it in your ac('record') call.

Note: You must create a visitor journey attribute of the Event name type. Do not use the Custom attribute type.

Test your solution in LiveNow:
1) Open Live Now.
2) Open your website.
3) Initiate a visit and go to the page that you are tracking.
4) Do nothing for 6 seconds.
5) Refresh Live Now and verify that a new user has been added to the TimeOnPage6Secs segment.